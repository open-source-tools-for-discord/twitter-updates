import discord,asyncio,os
from discord.ext import commands, tasks
import subprocess

token = '[METTRE LA CLE]'

class TwitterUpdates(discord.Client):
    async  def on_ready(self):
        self.change_status.start()
        print(f'{self.user} est disponible !')

    async def on_message(self,message):
        if message.author.id == self.user.id:
            return

        if message.content.startswith('!hello'):
            await message.reply ("Coucou! Je suis là!", mention_author=True)

    @tasks.loop(seconds=3450)
    async def change_status(self):
        subprocess.call(['sh','/var/discord/twitterupdates/[NOM DE VOTRE BOT DISCORD]/script.sh'])

        channel = self.get_channel([METTRE LE NUMERO DU SALON ICI])
        await self.change_presence(activity=discord.Game('TwitterUpdates : [NOM DU COMPTE]'))

        file1 = open('/var/discord/twitterupdates/[NOM DE VOTRE BOT DISCORD]/output.txt', 'r')
        Lines = file1.readlines()

        for line in Lines:
            await channel.send('Voici les informations trouvées : ' + line)

        file1.close()
        f = open("/var/discord/twitterupdates/output.txt", "w")
        f.close()

intents = discord.Intents.all()
intents.members = True
intents.message_content = True

bot = TwitterUpdates(intents=intents)
bot.run(token)
