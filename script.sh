#!/bin/sh

export TWINTDATE=`date --date="1 hour ago" +"%Y-%m-%d %H:%M:%S"`;

cd /var/discord/twitterupdates/[NOM DE VOTRE BOT DISCORD];
rm -R output.txt
rm -R outputnf_discord.txt
rm -R outputnf.txt

twint -u [NOM DU COMPTE] --since "$TWINTDATE" > outputnf.txt;

tac outputnf.txt > outputnf_discord.txt

grep -o 'https://t.[a-zA-Z.-]*/[a-zA-Z0-9+-]*' outputnf_discord.txt >> output.txt;
